import HttpDownloader from './HttpDownloader.js';
import MegaDownloader from './MegaDownloader.js';

/**
 * @typedef {typeof HttpDownloader|typeof MegaDownloader} DownloaderClass
 */

/** @type {Object<string,DownloaderClass>} */
const specialDownloader = {
  'mega.nz': MegaDownloader,
};

/**
 * @param {string} hostname
 * @returns {DownloaderClass}
 */
export function getDownloaderClassFor(hostname) {
  return specialDownloader[hostname] || HttpDownloader;
}

export function createDownloaderForUrl(url) {
  const { hostname } = new URL(url);
  const DownloaderClass = getDownloaderClassFor(hostname);
  return new DownloaderClass();
}
