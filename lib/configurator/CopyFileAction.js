import { copyFile } from 'fs/promises';
import { join, normalize } from 'path';
import Action from './Action.js';

/**
 * Configurator Action to copy a file from the configBaseDir to the IPA
 */
export default class CopyFileAction extends Action {

  /** @type {string} */
  #src;

  /** @type {string} */
  #dest;

  /**
   * @param {Object} config
   * @param {string} config.src
   * @param {string} config.dest
   */
  constructor({ src, dest, ...config }) {
    super(config);

    // Since we're dealing with local file locations in src and dest, but this
    // class could be created from configuration fetched from a remote resource,
    // we apply some extra care.
    if (typeof src !== 'string' || src.length === 0) {
      throw new Error(`Source path invalid: ${src}`);
    }
    if (normalize(src).match(/^(\.\.(\/|\\|$))+/)) {
      throw new Error(`Source path contains directory traversal: ${src}`);
    }
    if (typeof dest !== 'string' || dest.length === 0) {
      throw new Error(`Destination path invalid: ${dest}`);
    }
    if (normalize(dest).match(/^(\.\.(\/|\\|$))+/)) {
      throw new Error(`Destination path contains directory traversal: ${dest}`);
    }

    this.#src = src;
    this.#dest = dest;
  }

  /**
   * @param {string} ipaDir
   * @param {string} configBaseDir
   */
  async run(ipaDir, configBaseDir) {
    const cfgSrc = join(configBaseDir, this.#src);
    const cfgTarget = join(ipaDir, this.#dest);
    await copyFile(cfgSrc, cfgTarget);
  }

  /**
   * @returns {string}
   */
  toString() {
    return `Copy ${this.#src} to ${this.#dest}`;
  }

}
