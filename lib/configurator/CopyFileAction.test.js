import { expect } from 'chai';
import { readFileSync } from 'fs';
import mockfs from 'mock-fs';
import CopyFileAction from './CopyFileAction.js';

describe('configurator/CopyFileAction', () => {
  describe('#constructor', () => {
    // Since we're dealing with local file locations in `src` and `dest`, but this
    // class could be created from configuration fetched from a remote resource,
    // we apply some extra care.
    it('should throw if src is not a string', () => {
      const config = { src: [], dest: 'foo' };
      expect(() => new CopyFileAction(config)).to.throw(/^Source path invalid/);
    });
    it('should throw if src is an empty string', () => {
      const config = { src: '', dest: 'foo' };
      expect(() => new CopyFileAction(config)).to.throw(/^Source path invalid/);
    });
    it('should throw if src contains path traversal', () => {
      const config = { src: '../simple/traversal', dest: 'foo' };
      expect(() => new CopyFileAction(config)).to.throw(/^Source path contains directory traversal/);
    });

    it('should throw if dest is not a string', () => {
      const config = { src: 'foo', dest: null };
      expect(() => new CopyFileAction(config)).to.throw(/^Destination path invalid/);
    });
    it('should throw if dest is an empty string', () => {
      const config = { src: 'foo', dest: '' };
      expect(() => new CopyFileAction(config)).to.throw(/^Destination path invalid/);
    });
    it('should throw if dest contains path traversal', () => {
      const config = { src: 'foo', dest: 'complex/tra/../../../ver/sal' };
      expect(() => new CopyFileAction(config)).to.throw(/^Destination path contains directory traversal/);
    });

    it('should not throw if anything is fine', () => {
      const config = { src: 'foo', dest: 'bar' };
      expect(() => new CopyFileAction(config)).to.not.throw();
    });
  });

  describe('#run', () => {
    beforeEach(() => {
      mockfs({
        'source/path/config/file': 'some content',
        'dest/path': {},
      });
    });

    afterEach(() => {
      mockfs.restore();
    });

    it('should copy a file', async () => {
      const config = { src: 'config/file', dest: 'targetfile' };
      const action = new CopyFileAction(config);
      await action.run('dest/path', 'source/path');
      expect(readFileSync('dest/path/targetfile').toString()).to.be.equal('some content');
    });
  });

  describe('#toString', () => {
    it('should return a nice string', () => {
      const config = { src: 'foo', dest: 'bar' };
      const action = new CopyFileAction(config);
      expect(`${action}`).to.equal('Copy foo to bar');
    });
  });
});
