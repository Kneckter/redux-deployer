import ProgressMeter from '../util/ProgressMeter.js';

/**
 * The `IpaConfigurator` is being used to apply configuration actions to
 * an unpacked IPA package prior to the signing stage.
 */
export default class IpaConfigurator {

  /** @type {string} */
  #configBaseDir;

  /** @type {import('./Action */
  #actions;

  /**
   * Creates an IPA configurator which can apply the given config actions
   * @param {string} configBaseDir
   * @param {import('./Action actions
   */
  constructor(configBaseDir, actions) {
    this.#configBaseDir = configBaseDir;
    this.#actions = actions;
  }

  /**
   * Applies the configurator's actions
   * @param {string} ipaDir
   * @param {ProgressMeter} meter
   * @throws
   */
  async run(ipaDir, meter = new ProgressMeter()) {
    // eslint-disable-next-line no-restricted-syntax
    for (const [index, action] of this.#actions.entries()) {
      let error = null; // Store exception
      const percentage = index / this.#actions.length;
      let message = action.toString();

      try {
        // eslint-disable-next-line no-await-in-loop
        await action.run(ipaDir, this.#configBaseDir);
      } catch (/** @type {any} */ e) {
        if (!action.ignoreErrors) {
          error = e;
          throw e;
        }
        message = `${message} (ignore error: ${e.message})`;
      } finally {
        meter.set({ percentage, message, error });
      }
    }
  }

}
