import { expect } from 'chai';
import Action from './Action.js';
import { getActionClassFor, createActionsFromConfig } from './ActionFactory.js';

describe('configurator/ActionFactory', () => {
  describe('getActionClassFor', () => {
    it('should return a class', () => {
      const clazz = getActionClassFor('skip');
      expect(clazz).to.be.equal(Action);
    });
  });

  describe('createActionsFromConfig', () => {
    it('should return an array of Action classes according to config', () => {
      const config = [
        {
          action: 'skip',
        },
        {
          action: 'skip',
        },
      ];
      const actions = createActionsFromConfig(config);
      expect(actions).to.be.an('array').with.lengthOf(2);
      expect(actions[0]).to.be.instanceOf(Action);
      expect(actions[1]).to.be.instanceOf(Action);
    });
  });
});
