import Action from './Action.js';
import CopyFileAction from './CopyFileAction.js';
import PlistAction from './PlistAction.js';
import VersionAction from './VersionAction.js';

/**
 * @typedef {typeof Action
 * |typeof CopyFileAction
 * |typeof PlistAction
 * |typeof VersionAction} ActionClass
 */

/** @type {Object<string,ActionClass>} */
const actions = {
  skip: Action,
  copyFile: CopyFileAction,
  plist: PlistAction,
  version: VersionAction,
};

/**
 * @param {string} action
 * @returns {ActionClass}
 */
export function getActionClassFor(action) {
  return actions[action];
}

/**
 * One IpaConfiguratorAction can be created from a config object which
 * looks like this:
 * ```
 * {
 *   "action": ...,
 *   "parameter1": "value1",
 *   ....,
 *   "ignoreErrors": true/false
 * }
 * ```
 * The action is mapped to a corresponding action class which will receive
 * the config object in its constructor.
 *
 * @param {{action: string, ignoreErrors: boolean}} config
 * @returns {Action}
 */
function createActionFromConfig(config) {
  const ActionClass = getActionClassFor(config.action);
  return new ActionClass(config);
}

/**
 * The IpaConfiguratorActions can be created from an array of config objects,
 * whereas each object looks like this:
 * ```
 * {
 *   "action": ...,
 *   "parameter1": "value1",
 *   ....,
 *   "ignoreErrors": true/false
 * }
 * ```
 * Each action is mapped to a corresponding action class which will receive
 * the config object in its constructor.
 *
 * @param {Object[]} config
 * @returns {Action[]}
 */
export function createActionsFromConfig(config) {
  return config.map((step) => createActionFromConfig(step));
}
