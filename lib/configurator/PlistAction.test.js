import { expect, use } from 'chai';
import chaiAsPromised from 'chai-as-promised';
import mockfs from 'mock-fs';
import PlistAction from './PlistAction.js';

use(chaiAsPromised);

describe('configurator/PlistAction', () => {
  describe('#constructor', () => {
    // Since we're dealing with local file locations in `file` and dest, but this
    // class could be created from configuration fetched from a remote resource,
    // we apply some extra care.
    it('should throw if file is not a string', () => {
      const config = { file: [], command: 'abc', property: 'def' };
      expect(() => new PlistAction(config)).to.throw(/^File path invalid/);
    });
    it('should throw if src is an empty string', () => {
      const config = { file: '', command: 'abc', property: 'def' };
      expect(() => new PlistAction(config)).to.throw(/^File path invalid/);
    });
    it('should throw if file contains path traversal', () => {
      const config = { file: '../simple/traversal', command: 'abc', property: 'def' };
      expect(() => new PlistAction(config)).to.throw(/^File path contains directory traversal/);
    });

    it('should not throw if anything is fine', () => {
      const config = { file: 'test.plist', command: 'abc', property: 'def' };
      expect(() => new PlistAction(config)).to.not.throw();
    });
  });

  describe('#run', () => {
    const OPTIONS = { plistbuddyBinary: './test/plistbuddy-dummy.sh' };

    beforeEach(() => {
      mockfs({
        'dest/path': {},
      });
    });

    afterEach(() => {
      mockfs.restore();
    });

    it('should execute plistbuddy binary', async () => {
      const config = {
        file: 'test.plist', command: 'set', property: 'abc', type: 'integer', value: 3,
      };
      const action = new PlistAction(config, OPTIONS);
      await action.run('dest/path');
      // expect(fs.readFileSync('dest/path/targetfile').toString()).to.be.equal('some content');
    });

    it('should catch an generic error with exit code 1', () => {
      const config = {
        file: 'test.plist', command: 'print', property: ':exit1',
      };
      const action = new PlistAction(config, OPTIONS);
      return expect(action.run('dest/path')).to.be.rejected;
    });

    it('should catch an generic error  with exit code other than 1', () => {
      const config = {
        file: 'test.plist', command: 'print', property: ':exit2',
      };
      const action = new PlistAction(config, OPTIONS);
      return expect(action.run('dest/path')).to.be.rejected;
    });

    it('should catch a failed add', () => {
      const config = {
        file: 'test.plist', command: 'add', property: ':alreadyexisting', type: 'integer', value: 3,
      };
      const action = new PlistAction(config, OPTIONS);
      return expect(action.run('dest/path')).to.be.rejectedWith(/^Entry already exists/);
    });

    it('should catch a failed delete', () => {
      const config = {
        file: 'test.plist', command: 'delete', property: ':nonexisting',
      };
      const action = new PlistAction(config, OPTIONS);
      return expect(action.run('dest/path')).to.be.rejectedWith(/^Entry does not exist/);
    });
  });

  describe('#toString', () => {
    it('should return a nice string', () => {
      const config = { file: 'test.plist', command: 'delete', property: 'abc' };
      const action = new PlistAction(config);
      expect(`${action}`).to.equal('Modified test.plist: delete abc');
    });
  });
});
