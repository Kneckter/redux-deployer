import { expect, use } from 'chai';
import chaiAsPromised from 'chai-as-promised';
import mockfs from 'mock-fs';
import nock from 'nock';
import DeviceList from '../models/DeviceList.js';
import IpaTypeList from '../models/IpaTypeList.js';
import ReduxConfig from './ReduxConfig.js';

use(chaiAsPromised);

describe('ReduxConfig', () => {
  describe('#constructor()', () => {
    it('should create a ReduxConfig object with all getters to throw', () => {
      const reduxConfig = new ReduxConfig();
      expect(reduxConfig).to.be.instanceOf(ReduxConfig);
      expect(() => reduxConfig.versionOverride).to.throw('Not fully configured');
      expect(() => reduxConfig.deviceList).to.throw('Not fully configured');
      expect(() => reduxConfig.ipaTypeList).to.throw('Not fully configured');
    });
  });

  describe('#load()', () => {
    afterEach(() => {
      mockfs.restore();
    });

    it('should succeed with default devices.txt', async () => {
      mockfs({
        'config/settings.yaml': '---\n'
          + 'ipaTypes:\n'
          + '  - type: import\n'
          + '    file: redux.json\n',
        'config/redux.json': '{"ipaTypes": [{"id": "flowerpot"}]}',
        'config/devices.txt': 'deviceA:eed6657e924c869b79e0314f6dbf0255d62e3e5c:flowerpot',
      });

      const reduxConfig = new ReduxConfig();
      const settingsFile = 'config/settings.yaml';
      const resultPromise = reduxConfig.load({ settingsFile });

      await expect(resultPromise).to.eventually.be.fulfilled;
      expect(reduxConfig.deviceList).to.have.key('deviceA');
    });

    it('should succeed with devices.txt specified in settings.yaml', async () => {
      mockfs({
        'config/settings.yaml': '---\n'
          + 'deployment:\n'
          + '  devices:\n'
          + '    - type: import\n'
          + '      file: "../specified/in/settings/devices.txt"\n'
          + 'ipaTypes:\n'
          + '  - type: import\n'
          + '    file: redux.json\n',
        'config/redux.json': '{"ipaTypes": [{"id": "flowerpot"}]}',
        'specified/in/settings/devices.txt': 'deviceB:eed6657e924c869b79e0314f6dbf0255d62e3e5c:flowerpot',
      });

      const reduxConfig = new ReduxConfig();
      const settingsFile = 'config/settings.yaml';
      const resultPromise = reduxConfig.load({ settingsFile });

      await expect(resultPromise).to.eventually.be.fulfilled;
      expect(reduxConfig.deviceList).to.have.key('deviceB');
    });

    it('should succeed with devices.txt given as argument', async () => {
      mockfs({
        'config/settings.yaml': '---\n'
          + 'ipaTypes:\n'
          + '  - type: import\n'
          + '    file: redux.json\n',
        'config/redux.json': '{"ipaTypes": [{"id": "flowerpot"}]}',
        'argument/devices.txt': 'deviceC:eed6657e924c869b79e0314f6dbf0255d62e3e5c:flowerpot',
      });

      const reduxConfig = new ReduxConfig();
      const settingsFile = 'config/settings.yaml';
      const deviceFile = 'argument/devices.txt';
      const resultPromise = reduxConfig.load({ settingsFile, deviceFile });

      await expect(resultPromise).to.eventually.be.fulfilled;
      expect(reduxConfig.deviceList).to.have.key('deviceC');
    });

    it('should succeed with remote redux.json', async () => {
      mockfs({
        'config/settings.yaml': '---\n'
          + 'ipaTypes:\n'
          + '  - type: import\n'
          + '    file: http://localhost/redux.json\n',
        'config/devices.txt': 'deviceD:eed6657e924c869b79e0314f6dbf0255d62e3e5c:flowerpot',
      });
      nock('http://localhost').get('/redux.json').reply(200, '{"ipaTypes": [{"id": "flowerpot"}]}');

      const reduxConfig = new ReduxConfig();
      const settingsFile = 'config/settings.yaml';
      const resultPromise = reduxConfig.load({ settingsFile });

      await expect(resultPromise).to.eventually.be.fulfilled;
      expect(reduxConfig.deviceList).to.have.key('deviceD');
    });

    describe('with non-existing or empty config files', () => {
      it('should fail if settings.yaml does not exist', () => {
        mockfs({});

        const reduxConfig = new ReduxConfig();
        const settingsFile = 'invalid/settings.yaml';
        const resultPromise = reduxConfig.load({ settingsFile });

        return expect(resultPromise).to.eventually.be.rejectedWith(Error);
      });

      it('should fail if devices.txt does not exist', () => {
        mockfs({
          'config/settings.yaml': '---\n'
            + 'ipaTypes:\n'
            + '  - type: import\n'
            + '    file: redux.json\n',
          'config/redux.json': '{"ipaTypes": []}',
        });

        const reduxConfig = new ReduxConfig();
        const settingsFile = 'config/settings.yaml';
        const resultPromise = reduxConfig.load({ settingsFile });

        return expect(resultPromise).to.eventually.be.rejectedWith(Error);
      });

      it('should fail if local redux.json does not exist', () => {
        mockfs({
          'config/settings.yaml': '---\n'
            + 'ipaTypes:\n'
            + '  - type: import\n'
            + '    file: redux.json\n',
          'config/devices.txt': 'deviceC:eed6657e924c869b79e0314f6dbf0255d62e3e5c:flowerpot',
        });

        const reduxConfig = new ReduxConfig();
        const settingsFile = 'config/settings.yaml';
        const resultPromise = reduxConfig.load({ settingsFile });

        return expect(resultPromise).to.eventually.be.rejectedWith(Error);
      });

      it('should fail if local redux.json is empty', () => {
        mockfs({
          'config/settings.yaml': '---\n'
            + 'ipaTypes:\n'
            + '  - type: import\n'
            + '    file: redux.json\n',
          'config/devices.txt': 'deviceC:eed6657e924c869b79e0314f6dbf0255d62e3e5c:flowerpot',
          'config/redux.json': '',
        });

        const reduxConfig = new ReduxConfig();
        const settingsFile = 'config/settings.yaml';
        const resultPromise = reduxConfig.load({ settingsFile });

        return expect(resultPromise).to.eventually.be.rejectedWith(/SyntaxError/);
      });

      it('should fail if local redux.json lacks key "ipaTypes"', () => {
        mockfs({
          'config/settings.yaml': '---\n'
            + 'ipaTypes:\n'
            + '  - type: import\n'
            + '    file: redux.json\n',
          'config/devices.txt': 'deviceC:eed6657e924c869b79e0314f6dbf0255d62e3e5c:flowerpot',
          'config/redux.json': '{}',
        });

        const reduxConfig = new ReduxConfig();
        const settingsFile = 'config/settings.yaml';
        const resultPromise = reduxConfig.load({ settingsFile });

        return expect(resultPromise).to.eventually.be.rejectedWith(/No key "ipaTypes"/);
      });

      it('should fail if remote redux.json does not exist', () => {
        mockfs({
          'config/settings.yaml': '---\n'
            + 'ipaTypes:\n'
            + '  - type: import\n'
            + '    file: http://localhost/redux.json\n',
          'config/devices.txt': 'deviceC:eed6657e924c869b79e0314f6dbf0255d62e3e5c:flowerpot',
        });
        nock('http://localhost').get('/redux.json').reply(404);

        const reduxConfig = new ReduxConfig();
        const settingsFile = 'config/settings.yaml';
        const resultPromise = reduxConfig.load({ settingsFile });

        return expect(resultPromise).to.eventually.be.rejectedWith(Error);
      });

      it('should fail if remote redux.json is empty', () => {
        mockfs({
          'config/settings.yaml': '---\n'
            + 'ipaTypes:\n'
            + '  - type: import\n'
            + '    file: http://localhost/redux.json\n',
          'config/devices.txt': 'deviceC:eed6657e924c869b79e0314f6dbf0255d62e3e5c:flowerpot',
        });
        nock('http://localhost').get('/redux.json').reply(200, '');

        const reduxConfig = new ReduxConfig();
        const settingsFile = 'config/settings.yaml';
        const resultPromise = reduxConfig.load({ settingsFile });

        return expect(resultPromise).to.eventually.be.rejectedWith(/FetchError/);
      });
    });

    describe('with syntax errors in settings.yaml', () => {
      /**
       * @param {string} settingsYaml
       * @returns {Promise<void>}
       */
      const setupTestDummy = (settingsYaml) => {
        mockfs({
          'config/settings.yaml': settingsYaml,
        });

        const reduxConfig = new ReduxConfig();
        const settingsFile = 'config/settings.yaml';
        return reduxConfig.load({ settingsFile });
      };

      it('should fail with "invalid settings file" if it is empty', () => {
        const resultPromise = setupTestDummy('---\n');

        return expect(resultPromise).to.eventually.be.rejectedWith(/^Invalid settings file/);
      });

      it('should fail with "invalid settings file" if "ipaTypes" key does not exist', () => {
        const resultPromise = setupTestDummy('---\n'
          + 'deployment:\n'
          + '  devices:\n'
          + '    - type: import\n'
          + '      file: redux.json\n');

        return expect(resultPromise).to.eventually.be.rejectedWith(/^Invalid settings file/);
      });

      it('should fail with "invalid settings file" if "ipaTypes" key is not an array', () => {
        const resultPromise = setupTestDummy('---\n'
          + 'ipaTypes: invalid\n');

        return expect(resultPromise).to.eventually.be.rejectedWith(/^Invalid settings file/);
      });

      it('should fail with "invalid settings file" if "ipaTypes" key does lack an element with "type=import"', () => {
        const resultPromise = setupTestDummy('---\n'
          + 'ipaTypes:\n'
          + '  - type: invalid\n');

        return expect(resultPromise).to.eventually.be.rejectedWith(/^Invalid settings file/);
      });

      it('should fail with "invalid settings file" if "devices" key is not an array', () => {
        const resultPromise = setupTestDummy('---\n'
          + 'deployment:\n'
          + '  devices: invalid\n'
          + 'ipaTypes:\n'
          + '  - type: import\n'
          + '    file: redux.json\n');

        return expect(resultPromise).to.eventually.be.rejectedWith(/^Invalid settings file/);
      });

      it('should fail with "invalid settings file" if "devices" key does lack an element with "type=import"', () => {
        const resultPromise = setupTestDummy('---\n'
          + 'deployment:\n'
          + '  devices:\n'
          + '    - type: invalid\n'
          + 'ipaTypes:\n'
          + '  - type: import\n'
          + '    file: redux.json\n');

        return expect(resultPromise).to.eventually.be.rejectedWith(/^Invalid settings file/);
      });
    });

    describe('with inconsistent config options', () => {
      it('should fail with "Device list contains unknown IPA types"', () => {
        mockfs({
          'config/settings.yaml': '---\n'
            + 'ipaTypes:\n'
            + '  - type: import\n'
            + '    file: redux.json\n',
          'config/redux.json': '{"ipaTypes": []}',
          'config/devices.txt': 'deviceA:eed6657e924c869b79e0314f6dbf0255d62e3e5c:flowerpot',
        });

        const reduxConfig = new ReduxConfig();
        const settingsFile = 'config/settings.yaml';
        const resultPromise = reduxConfig.load({ settingsFile });

        return expect(resultPromise).to.eventually.be
          .rejectedWith(/^Device list contains unknown IPA types: flowerpot/);
      });
    });
  });

  describe('#getters', () => {
    /** @type {ReduxConfig} */
    let reduxConfig;

    beforeEach(async () => {
      mockfs({
        'config/settings.yaml': '---\n'
          + 'ipaTypes:\n'
          + '  - type: import\n'
          + '    file: redux.json\n'
          + 'signing:\n'
          + '  versionOverride: false\n'
          + '  developerProfile: "abc"\n'
          + '  provisioningProfile: "def.mobileprovision"\n'
          + '  keychainPassword: ghi\n'
          + 'deployment:\n'
          + '  concurrency: 3\n',
        'config/redux.json': '{"ipaTypes": [{"id": "flowerpot"}]}',
        'config/devices.txt': 'deviceFP:eed6657e924c869b79e0314f6dbf0255d62e3e5c:flowerpot',
      });
      reduxConfig = new ReduxConfig();
      const loadConfig = {
        settingsFile: 'config/settings.yaml',
        deviceFile: 'config/devices.txt',
      };
      await reduxConfig.load(loadConfig);
    });

    afterEach(() => {
      mockfs.restore();
    });

    it('"developerCertificate" should return name of developer certificate', () => {
      expect(reduxConfig.developerCertificate).to.be.equal('abc');
    });

    it('"provisioningProfile" should return path to provisioning profile in profile/ folder', () => {
      expect(reduxConfig.provisioningProfile).to.match(/\/config\/def\.mobileprovision$/);
    });

    it('"keychainPassword" should return password', () => {
      expect(reduxConfig.keychainPassword).to.be.equal('ghi');
    });

    it('"deployConcurrency" should return 3', () => {
      expect(reduxConfig.deployConcurrency).to.be.equal(3);
    });

    it('"versionOverride" should return 3', () => {
      expect(reduxConfig.versionOverride).to.be.equal(false);
    });

    it('"deviceList" should return a DeviceList', () => {
      expect(reduxConfig.deviceList).to.be.instanceOf(DeviceList);
    });

    it('"ipaTypeList" should return an IpaTypeList', () => {
      expect(reduxConfig.ipaTypeList).to.be.instanceOf(IpaTypeList);
    });

  });
});
