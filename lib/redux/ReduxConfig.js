import { join, dirname, resolve } from 'path';
import { readFile } from 'fs/promises';
import EventEmitter from 'events';
import fetch from 'node-fetch';
import yaml, { CORE_SCHEMA } from 'js-yaml';
import { URL } from 'url';
import DeviceList from '../models/DeviceList.js';
import IpaTypeList from '../models/IpaTypeList.js';

const moduleDirName = new URL('.', import.meta.url).pathname;

export const CONFIG_BASE_DIR = 'config';
export const RELEASES_BASE_DIR = '.ipa_cache';

/**
 * Reads JSON from a given local `filename` and decodes it.
 * @param {string} filename Local filename
 * @returns {Promise<any>} Decoded data
 * @throws {SyntaxError}
 */
async function readJsonFile(filename) {
  const content = (await readFile(filename)).toString();
  return JSON.parse(content);
}

/**
 * Reads JSON from the given remote `url` and decodes it.
 * @param {string} url Remote URL
 * @returns {Promise<any>} Decoded data
 * @throws {import('node-fetch').FetchError}
 */
async function fetchJson(url) {
  // @ts-ignore
  const response = await fetch(url);
  return response.json();
}

/**
 * Checks whether a given string is a local file name or a remote URL
 * @param {string} s
 * @returns {boolean}
 */
function isFilename(s) {
  try {
    const urlObject = new URL(s);
    return !urlObject.protocol.match(/^http/);
  } catch (e) {
    // do nothing
  }
  return true;
}

export default class ReduxConfig extends EventEmitter {

  /** @type {boolean} */
  #loaded = false;

  /** @type {string} */
  #deviceListFile = '';

  /** @type {DeviceList} */
  #deviceList = new DeviceList();

  /** @type {string} */
  #reduxConfigFile = '';

  /** @type {Object.<string,any>} */
  #reduxConfig = {};

  /** @type {IpaTypeList} */
  #ipaTypeList = new IpaTypeList();

  /** @type {string|false} */
  #versionOverride = false;

  /** @type {string} */
  #developerProfile = '';

  /** @type {string} */
  #provisioningProfile = '';

  /** @type {string|false} */
  #keychainPassword = false;

  /** @type {number} */
  #deployConcurrency = Number.POSITIVE_INFINITY;

  /**
   * @returns {string|false} Keychain password
   */
  get keychainPassword() {
    this.#checkLoaded();
    return this.#keychainPassword;
  }

  /**
   * @returns {number} Max. concurrency during deployment
   */
  get deployConcurrency() {
    this.#checkLoaded();
    return this.#deployConcurrency;
  }

  /**
   * @returns {string} Path to the provisioning profile
   */
  get provisioningProfile() {
    this.#checkLoaded();
    return join(moduleDirName, '..', '..', CONFIG_BASE_DIR, this.#provisioningProfile);
  }

  /**
   * @returns {string} Name of the Apple Developer Certificate to use for signing
   */
  get developerCertificate() {
    this.#checkLoaded();
    return this.#developerProfile;
  }

  /**
   * @returns {string|false} Version string to patch into IPAs, or false if not used
   */
  get versionOverride() {
    this.#checkLoaded();
    return this.#versionOverride;
  }

  /**
   * @returns {DeviceList}
   */
  get deviceList() {
    this.#checkLoaded();
    return this.#deviceList;
  }

  /**
   * @returns {IpaTypeList}
   */
  get ipaTypeList() {
    this.#checkLoaded();
    // @ts-ignore
    return this.#ipaTypeList;
  }

  /**
   * @param {Object} params
   * @param {string} params.settingsFile Path to the settings.yaml
   * @param {?string} params.deviceFile Path to the device.txt or device.json
   * @throws {Error}
   */
  async load({ settingsFile, deviceFile = null }) {
    // Helper functions
    // @ts-ignore
    const emitInfo = (progress, action) => this.emit('progress', { progress, action });

    // Load local settings
    await this.#loadSettings(settingsFile);
    emitInfo(25, `Loaded settings from ${settingsFile}`);

    // Load redux config data
    try {
      await this.#loadReduxConfig(this.#reduxConfigFile, dirname(settingsFile));
      emitInfo(50, `Loaded IPA type config from ${this.#reduxConfigFile}`);
      await this.#createIpaTypeList();
      const ipaInfo = this.#ipaTypeList.map((ipaType) => (ipaType.latestIpa
        ? `${ipaType.id} / ${ipaType.friendlyName} (available: ${ipaType.latestIpa.version})`
        : `${ipaType.id} / ${ipaType.friendlyName}`)).sort().join('\n  ');
      emitInfo(75, `Known IPA types:\n  ${ipaInfo}`);
    } catch (/** @type {any} */ e) {
      throw new Error(`${e.stack.replace(/^Error: /, '')}\nError: Invalid Redux config at ${this.#reduxConfigFile}`);
    }

    // Load device file
    const realDeviceFile = deviceFile || this.#getDeviceFilePath();
    await this.#loadDeviceList(realDeviceFile);
    // @ts-ignore
    emitInfo(100, `Loaded Device list with ${this.#deviceList.size} entries from ${realDeviceFile}`);

    this.#sanityCheck();

    this.#loaded = true;
  }

  /**
   * Performs a sanity cross-check on the data
   * @throws {Error}
   */
  #sanityCheck() {
    const availableIpaTypeIds = this.#ipaTypeList.map((ipaType) => ipaType.id);
    const deviceIpaTypeIds = this.#deviceList.getIpaTypeIds();
    const unknownDeviceIpaTypeIds = deviceIpaTypeIds
      .filter((ipaTypeId) => !availableIpaTypeIds.includes(ipaTypeId));
    if (unknownDeviceIpaTypeIds.length) {
      throw new Error(`Device list contains unknown IPA types: ${unknownDeviceIpaTypeIds.join(', ')}`);
    }
  }

  /**
   * Load the settings.yaml file
   * @param {string} settingsFile
   * @throws {import('js-yaml').YAMLException}
   */
  async #loadSettings(settingsFile) {
    const content = (await readFile(settingsFile)).toString();
    /** @type {Object.<string,any>} */
    const settings = yaml.load(content, {
      schema: CORE_SCHEMA,
    });

    if (!settings) {
      throw new Error(`Invalid settings file: ${settingsFile}`);
    }

    if (!settings.ipaTypes) {
      throw new Error(`Invalid settings file, missing "ipaTypes" key: ${settingsFile}`);
    }

    if (!settings.ipaTypes || !Array.isArray(settings.ipaTypes)) {
      throw new Error(`Invalid settings file, invalid "ipaTypes" key: ${settingsFile}`);
    }
    if (settings.ipaTypes?.[0]?.type !== 'import') {
      throw new Error(`Invalid settings file, improper "ipaTypes" list: ${settingsFile}`);
    }
    this.#reduxConfigFile = settings.ipaTypes[0].file;

    this.#provisioningProfile = settings.signing?.provisioningProfile;
    this.#developerProfile = settings.signing?.developerProfile;
    this.#keychainPassword = settings.signing?.keychainPassword || false;
    this.#versionOverride = settings.signing?.versionOverride || false;

    if (settings.deployment?.devices) {
      if (!Array.isArray(settings.deployment.devices)) {
        throw new Error(`Invalid settings file, invalid "devices" key: ${settingsFile}`);
      }
      if (settings.deployment.devices?.[0]?.type !== 'import') {
        throw new Error(`Invalid settings file, improper "devices" list: ${settingsFile}`);
      }
    }
    this.#deviceListFile = settings.deployment?.devices?.[0]?.file || 'devices.txt';

    const deployConcurrency = parseInt(settings.deployment?.concurrency, 10);
    this.#deployConcurrency = deployConcurrency || Number.POSITIVE_INFINITY;
  }

  /**
   * Reads the Redux config data from the given `url` (local filename or remote URL)
   * and populates the `_reduxConfig` field.
   * @param {string} url Local filename or remote URL
   * @param {string} settingsBasePath Base path of settings file
   * @throws {SyntaxError|import('node-fetch').FetchError}
   */
  async #loadReduxConfig(url, settingsBasePath) {
    const reduxConfig = isFilename(url)
      ? await readJsonFile(join(settingsBasePath, url))
      : await fetchJson(url);
    this.#reduxConfig = reduxConfig;
  }

  /**
   * Populates the `_ipaTypeList` field from the redux config
   * @throws
   */
  async #createIpaTypeList() {
    // Create IPA type list
    if (!this.#reduxConfig.ipaTypes) throw new Error('No key "ipaTypes"');
    const ipaTypeList = await IpaTypeList.loadFromConfig(this.#reduxConfig.ipaTypes);
    this.#ipaTypeList = ipaTypeList;
  }

  /**
   * Loads the Device list from the given file (txt or json format) and populates
   * the `_deviceList` field.
   * @param {string} deviceFile
   */
  async #loadDeviceList(deviceFile) {
    const content = (await readFile(deviceFile)).toString();
    const forceJson = !!deviceFile.match(/\.json$/);
    const deviceList = await DeviceList.loadFromString(content, { forceJson });
    this.#deviceList = deviceList;
  }

  /**
   * Throws error if the config isn't fully loaded
   * @throws
   */
  #checkLoaded() {
    if (!this.#loaded) {
      throw new Error('Not fully configured');
    }
  }

  /**
   * @returns {string} Absolute path to the devices.txt/devices.json
   */
  #getDeviceFilePath() {
    const configDir = join(moduleDirName, '..', '..', CONFIG_BASE_DIR);
    return resolve(configDir, this.#deviceListFile);
  }

}
