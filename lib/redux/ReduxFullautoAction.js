import { join } from 'path';
import Keychain from '../util/Keychain.js';
import { deploy } from './ReduxDeployAction.js';
import download from './ReduxDownloadAction.js';
import resign from './ReduxResignAction.js';
import { RELEASES_BASE_DIR } from './ReduxConfig.js';

/**
 * Automatically downloads, signs and deploys to the given devices
 * @param {Object} params
 * @param {import('./ReduxConfig').default} params.reduxConfig
 * @param {?boolean} params.local Don't download, use local file
 * @param {?string} params.url Force download from this URL
 * @param {?string} params.name If `url` is given, use this as local filename
 * @param {?string} params.version If give, override the IPA version
 * @param {boolean} params.reboot Automatically reboot after deployment
 */
export default async function fullAuto({
  reduxConfig,
  local = false,
  url = null,
  name = null,
  version = null,
  reboot = false,
}) {
  const keychain = new Keychain();
  keychain.unlockKeychain(reduxConfig.keychainPassword);

  const { deviceList } = reduxConfig;
  const deviceTypes = deviceList.getIpaTypeIds();

  // Download and re-sign
  if (url) {
    if (deviceTypes.length !== 1) {
      console.error("Custom URL supplied, but we don't know for which device type!");
      return;
    }
    const type = deviceTypes[0];
    const ipa = join(RELEASES_BASE_DIR, type, `${name}.ipa`);
    console.log('Custom URL supplied, downloading...');
    await download({
      reduxConfig,
      url,
      type,
      name,
    });
    await resign({
      reduxConfig,
      ipa,
      type,
      version,
    });
  } else {
    if (local) {
      console.log('Skip download, using local IPA...');
    } else {
      await download({ reduxConfig, deviceTypes });
    }
    await resign({ reduxConfig, deviceTypes, version });
  }

  if (deviceList.size === 0) {
    console.error('No devices configured. Setup your device file!');
    return;
  }

  // Start Deployment
  await deploy({ reduxConfig, deviceList, reboot });
}
