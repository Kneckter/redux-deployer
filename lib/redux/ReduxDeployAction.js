import { join } from 'path';
import pMap from 'p-map';
import { dir } from 'tmp-promise';
import IpaDeployer from '../idevice/IpaDeployer.js';
import IpaFinder from '../util/IpaFinder.js';
import IpaPacker from '../ipa/IpaPacker.js';
import ProgressScreen from '../ui/ProgressScreen.js';
import RetryDeployer from '../idevice/RetryDeployer.js';
import { rebootDevice } from './ReduxRebootAction.js';
import ProgressMeter from '../util/ProgressMeter.js';
import { RELEASES_BASE_DIR } from './ReduxConfig.js';

/**
 * Deploy ipa to device using ios-deploy
 * @param {import('../models/Device').default} device
 * @param {string} ipa
 * @param {number} retries
 * @param {boolean} reboot
 * @param {import('../ui/ProgressBar').default} progressBar
 * @throws
 */
async function deployIpa(device, ipa, retries, reboot, progressBar) {
  const { uuid } = device;
  const meter = new ProgressMeter({ progressBar });

  const retryCallback = async () => {
    progressBar.log('Deploy failed, restarting');
    await rebootDevice({ device, sleepTime: 20000, progressBar });
    progressBar.retryNext();
  };

  const retryDeployerOptions = {
    retries,
    retryCallback,
  };
  const deployer = new RetryDeployer(new IpaDeployer(), retryDeployerOptions);

  try {
    // Let's go!
    await deployer.deploy(uuid, ipa, meter);

    // Everything went successful
    progressBar.retryDone();
    if (reboot) {
      progressBar.log('Deploy complete, restarting');
      await rebootDevice({ device, progressBar });
    }
    progressBar.done('Deploy complete');
  } catch (e) {
    // This is terminal, we've retried enough times...
    progressBar.error('Deploy finally failed');
    throw e;
  }
}

/**
 * Deploys to the devices in the list. If `ipa` is specified, use it, otherwise use
 * latest IPA found in the releases directory which corresponds to the device's default
 * IPA type. All IPAs will be unpacked before deployment.
 * @param {Object} params
 * @param {import('../models/DeviceList').default} params.deviceList
 * @param {Object<string,string>} params.deployables Map of devices and deployables
 * @param {boolean} params.reboot Automaticall reboot after deployment
 * @param {number} params.concurrency
 * @param {number} params.retries
 * @returns {Promise<Object.<string,boolean>>} Map of device names and success value
 */
async function deployWithProgress({
  deviceList,
  deployables,
  reboot = false,
  concurrency,
  retries,
}) {
  const progress = new ProgressScreen(deviceList, { title: 'Deploy to devices' });

  // Deploy to the given device list
  /** @type {Object.<string,boolean>} */
  const results = {};
  /** @param {[any, import('../models/Device').default]} entry */
  const deployFn = async ([key, device]) => {
    const progressBar = progress.bar({ item: device, attempts: retries });
    const ipa = deployables[key];
    if (ipa === null) {
      progressBar.fail('Unknown IPA type');
      return;
    }
    progressBar.update(0.0, `Start deploying ${key}`, true);
    try {
      await deployIpa(device, ipa, retries, reboot, progressBar);
      results[device.name] = true;
      progress.removeBar(device);
    } catch (e) {
      results[device.name] = false;
    }
  };
  const promiseOptions = {
    stopOnError: false,
    concurrency,
  };
  await pMap(deviceList, deployFn, promiseOptions);

  progress.close();

  return results;
}

/**
 * Find he latest IPAs found in the releases directories of the given `ipaTypes`.
 * @param {string[]} ipaTypes
 * @returns {Promise<Object.<string,string>>} Map of ipaTypes and latest signed release
 */
async function findLatestSignedIpaReleases(ipaTypes) {
  const ipaFinder = new IpaFinder();

  /** @type {Object.<string,string>} */
  const ipaMap = {};

  await Promise.all(
    ipaTypes.map(async (type) => {
      const ipaBasePath = join(RELEASES_BASE_DIR, type);
      const pattern = join(ipaBasePath, '*Signed\\.ipa');
      const newestIpa = await ipaFinder.getNewestVersion(pattern);
      if (newestIpa === false) throw new Error(`No signed version for IPA type: ${type}`);
      ipaMap[type] = newestIpa;
    }),
  );

  return ipaMap;
}

/**
 * Unpacks the IPAs given in the ipaMap object into temporary directories.
 * @param {Object<string,string>} ipaMap Map of ipaTypes and latest signed release
 * @returns {Promise<Object.<string,string>>} Map of ipaTypes and unpacked release
 */
async function unpackIpasForDeployment(ipaMap) {
  /** @type {Object.<string,string>} */
  const unpackedIpaMap = {};

  console.log(`Unpack IPAs: ${Object.keys(ipaMap).join(', ')}`);
  await Promise.all(
    Object.keys(ipaMap).map(async (key) => {
      const { path: tmpIpaDir } = await dir({ unsafeCleanup: true });
      const ipaPacker = new IpaPacker();
      await ipaPacker.unpack(ipaMap[key], tmpIpaDir);
      unpackedIpaMap[key] = tmpIpaDir;
    }),
  );

  return unpackedIpaMap;
}

/**
 * Returns a textual feedback about failed deployments.
 * @param {Object.<string,boolean>} results Result of `deploy` function
 * @returns {string}
 */
export function formatDeployResult(results) {
  const resultArray = Object.entries(results);
  const failedDevices = resultArray
    .filter(([_, value]) => !value).map(([key, _]) => key)
    .sort();
  return (failedDevices.length
    ? `Deployment failed on ${failedDevices.join(', ')}.`
    : `Deployment successful on all ${resultArray.length} devices.`
  );
}

/**
 * Deploys to the devices in the list. If `ipa` is specified, use it, otherwise use
 * latest IPA found in the releases directory which corresponds to the device's default
 * IPA type. All IPAs will be unpacked before deployment.
 * @param {Object} params
 * @param {import('./ReduxConfig').default} params.reduxConfig
 * @param {import('../models/DeviceList').default} params.deviceList
 * @param {?string} params.ipa
 * @param {number} params.retries Number of attempts to install IPA on a device
 * @param {boolean} params.reboot Automatically reboot after deployment
 * @returns {Promise<Object.<string,boolean>>} Map of device names and success value
 */
export async function deploy({
  reduxConfig,
  deviceList,
  ipa = null,
  retries = 4,
  reboot = false,
}) {
  // Unpack all required IPAs
  const ipaMap = (ipa !== null)
    ? { [ipa]: ipa }
    : await findLatestSignedIpaReleases(deviceList.getIpaTypeIds());
  const unpackedIpaMap = await unpackIpasForDeployment(ipaMap);

  // Collect which unpacked IPA to deploy on which device
  /** @type {Object.<string,string>} */
  const deployables = {};
  deviceList.forEach((device, key) => {
    const ipaType = ipa || device.type || '';
    deployables[key] = unpackedIpaMap[ipaType];
  });

  const results = await deployWithProgress({
    deviceList,
    deployables,
    reboot,
    concurrency: reduxConfig.deployConcurrency,
    retries,
  });
  console.log(formatDeployResult(results));

  return results;
}
