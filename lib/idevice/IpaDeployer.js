import { spawn } from 'child_process';
import { basename, join } from 'path';
import EventEmitter from 'events';
import { URL } from 'url';
import ProgressMeter from '../util/ProgressMeter.js';

const dirname = new URL('.', import.meta.url).pathname;

const IOSDEPLOY_BINARY = join(dirname, '..', '..', 'node_modules', '.bin', 'ios-deploy');

/**
 * Deploys an IPA to a device using ios-deploy
 */
export default class IpaDeployer extends EventEmitter {

  #binary;

  #timeout;

  /**
   * @param {Object} options
   * @param {string} options.iosdeployBinary
   * @param {number} options.timeout
   */
  constructor({ iosdeployBinary = IOSDEPLOY_BINARY, timeout = 30 } = {}) {
    super();

    this.#binary = iosdeployBinary;
    this.#timeout = timeout;
  }

  /**
   * Deploys an IPA file to the device with the given uuid
   * @param {string} uuid Device ID
   * @param {string} ipa Path to IPA file or unpacked IPA directory
   * @param {ProgressMeter} meter
   * @returns {Promise<object>} object = { uuid, ipa }
   */
  deploy(uuid, ipa, meter = new ProgressMeter()) {
    return new Promise((resolve, reject) => {
      const iosdeployParams = [
        '--bundle', ipa,
        '--id', uuid,
        '--timeout', `${this.#timeout}`,
      ];
      const child = spawn(this.#binary, Object.freeze(iosdeployParams));

      let percentage = 0.0;

      child.on('close', (code) => {
        if (code === 0) {
          resolve({ uuid, ipa });
        } else {
          reject(new Error(`ios-deploy exited with code ${code}`));
        }
      });

      child.on('error', (error) => {
        reject(error);
      });

      child.stdout.on('data', (buffer) => {
        buffer.toString().trim().split(/\n/).forEach((/** @type {string} */ output) => {
          if (output.match(/^\[\.\.\.\.\] Using /)) {
            meter.set({ percentage: 0.0, message: 'Deploy', file: null });
          }
          const parsedOutput = output.match(/^\[\s*(\d+)%\] (?:Copying (.+) to device)?/);
          if (parsedOutput) {
            // eslint-disable-next-line no-unused-vars
            const [_, progressString, file] = parsedOutput;
            if (progressString) {
              percentage = Number.parseInt(progressString, 10) / 100;
              const fileInfo = file ? ` (${basename(file)})` : '';
              meter.set({ percentage, message: `Deploy${fileInfo}`, file });
            }
          }
        });
      });

      child.stderr.on('data', (buffer) => {
        const output = buffer.toString().trim();
        if (output.match(/Timed out waiting for device/)) {
          reject(new Error('Timed out waiting for device'));
          child.kill();
        } else {
          meter.error(output);
        }
      });
    });
  }

  /**
   * Removes a IPA from the device with the given uuid.
   * @param {string} uuid Device ID
   * @param {string} bundle AppId of the bundle to remove
   * @param {ProgressMeter} meter
   * @returns {Promise<object>} object = { uuid, bundle }
   */
  remove(uuid, bundle, meter = new ProgressMeter()) {
    return new Promise((resolve, reject) => {
      const iosdeployParams = [
        '--uninstall_only',
        '--bundle_id', bundle,
        '--id', uuid,
        '--timeout', `${this.#timeout}`,
      ];
      const child = spawn(this.#binary, Object.freeze(iosdeployParams));

      child.on('close', (code) => {
        if (code === 0 || code === null) {
          resolve({ uuid, bundle });
        } else {
          reject(new Error(`ios-deploy exited with code ${code}`));
        }
      });

      child.on('error', (error) => {
        reject(error);
      });

      child.stdout.on('data', (buffer) => {
        buffer.toString().trim().split(/\n/).forEach((/** @type {string} */ output) => {
          if (output.match(/^\[\.\.\.\.\] Using /)) {
            meter.set({ percentage: 0.0, bundle });
          }
          if (output.match(/^------ Uninstall phase ------/)) {
            meter.set({ percentage: 0.2, bundle });
          }
          if (output.match(/^\[ OK \] Uninstalled package with bundle id/)) {
            meter.set({ percentage: 1.0, bundle });
          }
        });
      });

      child.stderr.on('data', (buffer) => {
        const output = buffer.toString().trim();
        if (output.match(/Timed out waiting for device/)) {
          reject(new Error('Timed out waiting for device'));
          child.kill();
        } else {
          meter.error(output);
        }
      });
    });
  }

  /**
   * @param {Object} options
   * @param {AbortSignal|undefined} options.abortSignal
   * @emits IpaDeployer#deviceFound
   * @returns {Promise<Array<object>>}
   */
  scan({ abortSignal = undefined } = {}) {
    return new Promise((resolve, reject) => {
      const iosdeployParams = ['--detect', '--timeout', `${this.#timeout}`];
      const spawnOptions = { signal: abortSignal };
      const child = spawn(this.#binary, Object.freeze(iosdeployParams), spawnOptions);

      /** @type {Object[]} */
      const result = [];

      child.on('close', (code) => {
        if (code === 0) {
          resolve(result);
        } else {
          reject(new Error(`ios-deploy exited with code ${code}`));
        }
      });

      child.on('error', (error) => {
        reject(error);
      });

      child.stdout.on('data', (buffer) => {
        buffer.toString().trim().split(/\n/).forEach((/** @type {string} */ output) => {
          const parsedOutput = output.match(/^\[\.\.\.\.\] Found ([0-9a-fA-F-]+) .+?a\.k\.a\. '(.+?)' connected through (USB|WIFI)\./);
          if (parsedOutput) {
            // eslint-disable-next-line no-unused-vars
            const [_, uuid, name, port] = parsedOutput;
            const device = { uuid, name, port: port.toLowerCase() };
            /**
             * @event IpaDeployer#deviceFound
             * @type {Object}
             * @property {string} uuid
             * @property {string} name
             * @property {string} port
             */
            this.emit('deviceFound', device);
            result.push(device);
          }
        });
      });

      child.stderr.on('data', (buffer) => {
        const output = buffer.toString().trim();
        this.emit('error', { output });
      });
    });
  }

}
