import EventEmitter from 'events';
import { expect, use } from 'chai';
import chaiAsPromised from 'chai-as-promised';
import Device from '../models/Device.js';
import DeviceList from '../models/DeviceList.js';
import DevicePortScanner from './DevicePortScanner.js';

use(chaiAsPromised);

describe('DevicePortScanner', () => {
  describe('constructor', () => {
    describe('called with default parameters', () => {
      const scanner = new DevicePortScanner(new DeviceList());

      it('should return a DevicePortScanner object with a default IpaDeployer', () => {
        expect(scanner).to.be.instanceOf(DevicePortScanner);
      });
    });
  });

  describe('#run', () => {
    /** @type {Device} */
    let deviceA;
    /** @type {Device} */
    let deviceB;
    /** @type {Device} */
    let deviceC;
    /** @type {DeviceList} */
    let deviceList;
    /** @type {EventEmitter} */
    let ipaDeployer;

    const setupTestDummy = (scanFn = () => {}) => {
      // Setup a few Devices
      deviceA = new Device('a');
      deviceB = new Device('b');
      deviceC = new Device('c');

      // Setup a DeviceList
      deviceList = new DeviceList().set('a', deviceA).set('b', deviceB).set('c', deviceC);

      // Create IpaDeployer dummy
      ipaDeployer = new EventEmitter();
      // @ts-ignore
      ipaDeployer.scan = scanFn;

      // Setup actual DevicePortScanner
      // @ts-ignore
      return new DevicePortScanner(deviceList, { ipaDeployer });
    };

    it('should update all devices with port information the IpaDeployer emitted', async () => {
      // Create test dummy
      const scanner = setupTestDummy();

      // Run test
      const resultPromise = scanner.run();
      ipaDeployer.emit('deviceFound', { name: 'a', port: 'wifi' });
      ipaDeployer.emit('deviceFound', { name: 'a', port: 'usb' }); // same device with 'usb' should override previous 'wifi'
      ipaDeployer.emit('deviceFound', { name: 'b', port: 'usb' });
      ipaDeployer.emit('deviceFound', { name: 'b', port: 'wifi' }); // same device with 'wifi' should NOT override previous 'usb'
      ipaDeployer.emit('deviceFound', { name: 'x', port: 'wifi' }); // unknown device should not have an influence
      ipaDeployer.emit('deviceFound', { name: 'c', port: 'wifi' });
      const result = await resultPromise;

      expect(deviceA.port).to.be.equal('usb');
      expect(deviceB.port).to.be.equal('usb');
      expect(deviceC.port).to.be.equal('wifi');
      expect(result).to.be.true;
    });

    describe('with AbortController able to abort scan', () => {
      // Define a function which mimics IpaDeployer#scan to be aborted
      /** @param {{ abortSignal: AbortSignal }} options */
      // eslint-disable-next-line no-param-reassign
      const scanFn = ({ abortSignal }) => new Promise(
        (_res, rej) => { abortSignal.onabort = rej; },
      );

      const setupNoKnownPortsTestDummy = () => {
        const scanner = setupTestDummy(scanFn);
        return scanner;
      };

      const setupSomeUnknownPortsTestDummy = () => {
        const scanner = setupTestDummy(scanFn);
        deviceA.port = 'usb';
        deviceB.port = 'usb';
        return scanner;
      };

      const setupAllKnownPortsTestDummy = () => {
        const scanner = setupTestDummy(scanFn);
        deviceA.port = 'usb';
        deviceB.port = 'usb';
        deviceC.port = 'usb';
        return scanner;
      };

      describe('if called when no ports are known yet', () => {
        it('should return false if AbortController.abort is called before anything is scanned', () => {
          // Create test dummy
          const scanner = setupNoKnownPortsTestDummy();

          // Run test
          const abort = new AbortController();
          const resultPromise = scanner.run(abort);
          abort.abort();

          return expect(resultPromise).to.eventually.be.false;
        });

        it('should return false if AbortController is already aborted', () => {
          // Create test dummy
          const scanner = setupNoKnownPortsTestDummy();

          // Run test
          const abort = new AbortController();
          abort.abort();
          const resultPromise = scanner.run(abort);

          return expect(resultPromise).to.eventually.be.false;
        });
      });

      describe('if called when some ports are yet unknown', () => {
        it('should return false if AbortController.abort is called before anything is scanned', () => {
          // Create test dummy
          const scanner = setupSomeUnknownPortsTestDummy();

          // Run test
          const abort = new AbortController();
          const resultPromise = scanner.run(abort);
          abort.abort();

          return expect(resultPromise).to.eventually.be.false;
        });

        it('should return false if AbortController is already aborted', () => {
          // Create test dummy
          const scanner = setupSomeUnknownPortsTestDummy();

          // Run test
          const abort = new AbortController();
          abort.abort();
          const resultPromise = scanner.run(abort);

          return expect(resultPromise).to.eventually.be.false;
        });
      });

      describe('if called when all ports are known', () => {
        it('should return true if AbortController.abort is called before anything is scanned', () => {
          // Create test dummy
          const scanner = setupAllKnownPortsTestDummy();

          // Run test
          const abort = new AbortController();
          const resultPromise = scanner.run(abort);
          abort.abort();

          return expect(resultPromise).to.eventually.be.true;
        });

        it('should return true if AbortController is already aborted', () => {
          // Create test dummy
          const scanner = setupAllKnownPortsTestDummy();

          // Run test
          const abort = new AbortController();
          abort.abort();
          const resultPromise = scanner.run(abort);

          return expect(resultPromise).to.eventually.be.true;
        });
      });
    });
  });
});
