import Device from '../models/Device.js';
import IpaDeployer from './IpaDeployer.js';

/**
 * Scan for connected iDevices and updates the Device objects in the given DeviceList
 * with the correct port information (Wifi or USB). If a device is found on both,
 * USB takes precedence.
 */
export default class DevicePortScanner {

  /**
   * @type {import('../models/DeviceList').default}
   */
  #deviceList;

  /**
   * @type {IpaDeployer}
   */
  #ipaDeployer;

  /**
   * @param {import('../models/DeviceList').default} deviceList
   * @param {Object} options
   * @param {IpaDeployer} options.ipaDeployer
   */
  constructor(deviceList, { ipaDeployer = new IpaDeployer() } = {}) {
    this.#deviceList = deviceList;
    this.#ipaDeployer = ipaDeployer;
  }

  /**
   * Starts the scanner. The AbortController allows to stop the scanner before the ios-deploy
   * timeout
   * @param {AbortController} abort
   * @returns {Promise<boolean>} Resolves with success state. Never rejects!
   */
  async run(abort = new AbortController()) {
    // The AbortController's signal must not be aborted
    if (abort.signal.aborted) return this.#allUpdated();

    // Setup update listener and start scanning
    this.#setupDeviceUpdateListener(abort);
    try {
      await this.#ipaDeployer.scan({ abortSignal: abort.signal });
    } catch (e) {
      // do nothing
    }

    // Make sure that scanner process is terminated
    abort.abort();

    // Scan run is successful if no devices are without port
    return this.#allUpdated();
  }

  /**
   * Attaches a listener to the `deviceFound` signal of the `_ipaDeployer` which then
   * updates the corresponding `device` object. Scan is terminated via the passed
   * `AbortController`.
   * @param {AbortController} abort
   */
  #setupDeviceUpdateListener(abort) {
    /**
     * @param {Object} Object
     * @param {string} Object.name
     * @param {string} Object.port
     */
    const deviceUpdater = ({ name: key, port }) => {
      const device = this.#deviceList.get(key);
      // Update devices with unknown port or wifi port (as we prefer USB over Wifi)
      if (device && (!device.port || device.port === Device.PORT_WIFI)) {
        device.port = port;
      }
      // Stop run if all devices are covered
      if (this.#allUpdated()) {
        abort.abort();
      }
    };

    // Add the listener
    this.#ipaDeployer.addListener('deviceFound', deviceUpdater);

    // Remove the listener if `AbortController` is triggered
    abort.signal.addEventListener('abort', () => {
      this.#ipaDeployer.removeListener('deviceFound', deviceUpdater);
    }, { once: true });
  }

  /**
   * @returns {boolean} True if all devices have their port scanned
   */
  #allUpdated() {
    // All devices have their port = no device has a null port
    return !this.#deviceList.getPorts().null;
  }

}
