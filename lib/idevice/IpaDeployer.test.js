import { expect, use } from 'chai';
import chaiAsPromised from 'chai-as-promised';
import IpaDeployer from './IpaDeployer.js';

use(chaiAsPromised);

class ProgressMeterDummy {

  constructor() {
    /** @type {number[]} */
    this.result = [];
  }

  set(o) {
    this.result.push(o);
  }

}

describe('IpaDeployer', () => {
  describe('#deploy()', () => {
    it('should eventually succeed if valid binary is invoked', () => {
      const deployer = new IpaDeployer({ iosdeployBinary: './test/ios-deploy-dummy.sh' });
      return expect(deployer.deploy('abc', 'def.ipa')).to.eventually.be.deep.equal({ uuid: 'abc', ipa: 'def.ipa' });
    });

    it('should emit a 100% progress event at the end', async () => {
      const meter = new ProgressMeterDummy();
      const deployer = new IpaDeployer({ iosdeployBinary: './test/ios-deploy-dummy.sh' });
      // @ts-ignore
      await deployer.deploy('abc', 'def.ipa', meter);
      return expect(meter.result.at(-1)).to.include({ percentage: 1.0 });
    });

    it('should fail if invalid binary is invoked', () => {
      const deployer = new IpaDeployer({ iosdeployBinary: 'non-existing-binary' });
      return expect(deployer.deploy('abc', 'def.ipa')).to.eventually.be.rejected;
    });
  });

  describe('#remove()', () => {
    it('should eventually succeed if valid binary is invoked', () => {
      const deployer = new IpaDeployer({ iosdeployBinary: './test/ios-deploy-dummy.sh' });
      return expect(deployer.remove('abc', 'def')).to.eventually.be.deep.equal({ uuid: 'abc', bundle: 'def' });
    });

    it('should emit a 100% progress event at the end', async () => {
      const meter = new ProgressMeterDummy();
      const deployer = new IpaDeployer({ iosdeployBinary: './test/ios-deploy-dummy.sh' });
      // @ts-ignore
      await deployer.remove('abc', 'def', meter);
      return expect(meter.result.at(-1)).to.include({ percentage: 1.0 });
    });

    it('should fail if invalid binary is invoked', () => {
      const deployer = new IpaDeployer({ iosdeployBinary: 'non-existing-binary' });
      return expect(deployer.remove('abc', 'def')).to.eventually.be.rejected;
    });
  });

  describe('#scan()', () => {
    it('should eventually succeed if valid binary is invoked', () => {
      const deployer = new IpaDeployer({ iosdeployBinary: './test/ios-deploy-dummy.sh' });
      return expect(deployer.scan()).to.eventually.be.an('array').with.length(10);
    });

    it('should emit deviceFound events', async () => {
      const deployer = new IpaDeployer({ iosdeployBinary: './test/ios-deploy-dummy.sh' });
      /** @type {import('../models/Device').default[]} */
      const collect = [];
      deployer.on('deviceFound', (device) => collect.push(device));
      await deployer.scan();
      expect(collect).to.have.lengthOf(10);
      expect(collect[0]).to.be.an('object').that.has.all.keys('name', 'uuid', 'port');
      expect(collect[0].port).to.equals('wifi');
      expect(collect[1]).to.be.an('object').that.has.all.keys('name', 'uuid', 'port');
      expect(collect[1].port).to.equals('usb');
    });

    it('should fail if invalid binary is invoked', () => {
      const deployer = new IpaDeployer({ iosdeployBinary: 'non-existing-binary' });
      return expect(deployer.scan()).to.eventually.be.rejected;
    });
  });
});
