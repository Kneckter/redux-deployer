import ProgressMeter from '../util/ProgressMeter.js';

export default class RetryDeployer {

  /**
   * @type {import('./IpaDeployer').default}
   */
  #deployer;

  /**
   * @type {number}
   */
  #maxRetries;

  /**
   * @type {function}
   */
  #retryCallback;

  /**
   * @type {number}
   */
  #attempt;

  /**
   * @param {import('./IpaDeployer').default} deployer The IpaDeployer object
   * @param {Object} options
   * @param {number} options.retries Number of retry attempts
   * @param {function} options.retryCallback Callback to execute and wait for between retriess
   */
  constructor(deployer, { retries = 3, retryCallback = async () => {} } = {}) {
    this.#deployer = deployer;
    this.#maxRetries = retries;
    this.#retryCallback = retryCallback;

    this.#attempt = 1;
  }

  /**
   * @returns {number}
   */
  get attempt() {
    return this.#attempt;
  }

  /**
   * Calls the IPA deployer with retry logic
   * @param {string} uuid Device ID
   * @param {string} ipa Path to IPA file or unpacked IPA directory
   * @param {ProgressMeter} meter
   * @returns {Promise<object>}
   */
  async deploy(uuid, ipa, meter = new ProgressMeter()) {
    for (this.#attempt = 1; ; this.#attempt++) {
      try {
        // eslint-disable-next-line no-await-in-loop
        const res = await this.#deployer.deploy(uuid, ipa, meter);
        return { ...res, attempt: this.#attempt, maxAttempts: this.#maxRetries };
      } catch (/** @type {any} */ e) {
        if (this.#attempt === this.#maxRetries) {
          throw e;
        }
        meter.retry(`Deploy failed: ${e}`, e);
        // eslint-disable-next-line no-await-in-loop
        await this.#retryCallback();
      }
    }
  }

}
