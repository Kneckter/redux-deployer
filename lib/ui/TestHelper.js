// @ts-nocheck
import DeviceSelect from './DeviceSelect.js';
import IpaTypeSelect from './IpaTypeSelect.js';
import Menu from './Menu.js';
import YesNoToggle from './YesNoToggle.js';

const DEFAULT_CLASSES = [
  DeviceSelect,
  IpaTypeSelect,
  Menu,
  YesNoToggle,
];

/**
 * Disables the render method on the given `enquirer` Prompt classes
 * @param {import('enquirer').Prompt[]} Clazzes
 */
export function disableEnquirerRender(Clazzes = DEFAULT_CLASSES) {
  Clazzes.forEach((EnquirerPromptClass) => {
    if (typeof EnquirerPromptClass.prototype.render.old === 'undefined') {
      const noRender = () => {};
      noRender.old = EnquirerPromptClass.prototype.render;
      EnquirerPromptClass.prototype.render = noRender;
    }
  });
}

/**
 * Restores the render method on the given `enquirer` Prompt classes
 * @param {import('enquirer').Prompt[]} Clazzes
 */
export function enableEnquirerRender(Clazzes = DEFAULT_CLASSES) {
  Clazzes.forEach((EnquirerPromptClass) => {
    if (typeof EnquirerPromptClass.prototype.render.old !== 'undefined') {
      EnquirerPromptClass.prototype.render = EnquirerPromptClass.prototype.render.old;
    }
  });
}
