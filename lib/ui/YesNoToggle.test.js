import { expect, use } from 'chai';
import chaiAsPromised from 'chai-as-promised';
import { disableEnquirerRender, enableEnquirerRender } from './TestHelper.js';
import YesNoToggle from './YesNoToggle.js';

use(chaiAsPromised);

describe('interactive/YesNoToggle', () => {
  beforeEach(() => {
    disableEnquirerRender();
  });

  afterEach(() => {
    enableEnquirerRender();
  });

  describe('constructor', () => {
    it('should work with default options', () => {
      const prompt = new YesNoToggle();
      expect(prompt.options.message).to.be.undefined;
    });

    it('should work with a title (message) option', () => {
      const prompt = new YesNoToggle({ message: 'title' });
      expect(prompt.options.message).to.be.equal('title');
    });
  });

  describe('#run', () => {
    /** @returns {YesNoToggle} */
    const setupTestDummy = (options, testInteractions) => {
      const prompt = new YesNoToggle(options);
      // Perform testInteractions once `run` event fires
      prompt.once('run', () => {
        testInteractions.forEach((el) => prompt[el]());
      });
      return prompt;
    };

    it('should return false if "No" (default) is selected', () => {
      const prompt = setupTestDummy(undefined, ['submit']);
      return expect(prompt.run()).to.eventually.be.equal(false);
    });

    it('should return true if "Yes" is selected', () => {
      const prompt = setupTestDummy(undefined, ['toggle', 'submit']);
      return expect(prompt.run()).to.eventually.be.equal(true);
    });

    it('should reject if interaction is canceled', () => {
      const prompt = setupTestDummy(undefined, ['cancel']);
      return expect(prompt.run()).to.eventually.be.rejected;
    });
  });
});
