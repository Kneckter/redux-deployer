/**
 * Thrown if the user wants to exit
 */
export default class InteractionExitError extends Error {
}
