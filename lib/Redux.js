import { join } from 'path';
import commandLineArgs from 'command-line-args';
import DevicePortScanner from './idevice/DevicePortScanner.js';
import InteractionController from './ui/InteractionController.js';
import ReduxConfig, { CONFIG_BASE_DIR } from './redux/ReduxConfig.js';
import fullAuto from './redux/ReduxFullautoAction.js';
import { reboot } from './redux/ReduxRebootAction.js';
import remove from './redux/ReduxRemoveAction.js';

// Command line arguments
const optionDefinitions = [
  {
    name: 'ipa', alias: 'i', type: String, defaultValue: null,
  },
  {
    name: 'devices', alias: 'd', type: String, defaultValue: null,
  },
  {
    name: 'restartAll', alias: 'r', type: Boolean,
  },
  {
    name: 'cleanAll', alias: 'c', type: Boolean,
  },
  {
    name: 'fullAuto', alias: 'f', type: Boolean,
  },
  {
    name: 'local', alias: 'l', type: Boolean,
  },
  {
    name: 'url', alias: 'u', type: String, defaultValue: null,
  },
  {
    name: 'name', alias: 'n', type: String, defaultValue: 'Custom',
  },
  {
    name: 'version', alias: 'v', type: String, defaultValue: null,
  },
];

const options = commandLineArgs(optionDefinitions);

/**
 * Handles non-interactive invocation
 * @param {ReduxConfig} reduxConfig
 * @param {Promise<boolean>} scannerPromise
 */
async function noninteractive(reduxConfig, scannerPromise) {
  const { deviceList } = reduxConfig;

  // Before we do anything, wait for the scanner to finish
  const scanSuccess = await scannerPromise;
  if (!scanSuccess) {
    const names = deviceList.filter(([_, device]) => !device.port).map((device) => device.name);
    console.error(`Couldn't find some devices: ${names.join(', ')}`);
  }

  if (options.cleanAll) {
    await remove({ reduxConfig, deviceList });
    console.log('All devices got cleaned.');
  }
  if (options.restartAll) {
    await reboot({ deviceList, sleepTime: 20000 });
    console.log('All devices got restarted.');
  }
  if (options.fullAuto) {
    await fullAuto({
      reduxConfig,
      local: options.local,
      url: options.url,
      name: options.name,
      version: options.version,
      reboot: options.cleanAll, // Reboot if "cleanAll" is given (TODO: why???)
    });
  }
}

/**
 * Handles interactive invocation
 * @param {ReduxConfig} reduxConfig
 */
async function interactive(reduxConfig) {
  const iac = new InteractionController({
    optionIpa: options.ipa,
    optionVersion: options.version,
    reduxConfig,
  });
  try {
    await iac.run();
    console.log('Exiting');
  } catch (e) {
    console.error(e);
  }
}

/**
 * This is the `main` function.
 */
export default async function main() {
  const scanAbort = new AbortController();
  try {
    const reduxConfig = new ReduxConfig();
    reduxConfig.on('progress', ({ action }) => console.log(action));
    await reduxConfig.load({
      settingsFile: join(CONFIG_BASE_DIR, 'settings.yaml'),
      deviceFile: options.devices,
    });

    // Start a device port scanner (running in the background until all device ports are known)
    const scanner = new DevicePortScanner(reduxConfig.deviceList);
    console.log('Start scanning for devices on USB and Wifi...');
    const scannerPromise = scanner.run(scanAbort);

    if (options.cleanAll || options.restartAll || options.fullAuto) {
      await noninteractive(reduxConfig, scannerPromise);
    } else {
      await interactive(reduxConfig);
    }
  } catch (/** @type {any} */ e) {
    if (e.errno === -2 && e.code === 'ENOENT') {
      console.error(`Error: File not found: ${e.path}`);
    } else {
      console.error(e);
    }
  } finally {
    scanAbort.abort();
  }
}
