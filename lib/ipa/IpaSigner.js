import { promisify } from 'util';
import { join, parse } from 'path';
import {
  writeFile,
  copyFile,
  rename,
  rm,
} from 'fs/promises';
import { execFile as execFileCb } from 'child_process';
import fastglob from 'fast-glob';
import { setGracefulCleanup, dir as tmpDir, file as tmpFile } from 'tmp-promise';
import IpaPacker from './IpaPacker.js';
import ProgressMeter from '../util/ProgressMeter.js';

const execFile = promisify(execFileCb);

const PLISTBUDDY_BINARY = join('/usr', 'libexec', 'PlistBuddy');
const SECURITY_BINARY = join('/usr', 'bin', 'security');
const CODESIGN_BINARY = join('/usr', 'bin', 'codesign');

/**
 * Returns the first `.app` folder within the `Payload` folder of `ipaDir`
 * @param {string} ipaDir Path ot the extracted IPA to re-sign
 * @returns {Promise<string>} Path of the `.app` folder
 */
async function getFirstPayloadAppDir(ipaDir) {
  const payloadDir = `${ipaDir}/Payload/`;
  const globOptions = {
    cwd: payloadDir,
    nocase: true,
    deep: 1,
    absolute: true,
    onlyFiles: false,
    onlyDirectories: true,
  };
  return (await fastglob('*.app', globOptions))[0];
}

/**
 * Removes all __MACOSX folders below `baseDir`
 * @param {string} baseDir
 * @returns {Promise<number>} Number of removed folders
 */
async function removeMacOsResourceDirs(baseDir) {
  const globOptions = {
    cwd: baseDir,
    absolute: true,
    onlyFiles: false,
    onlyDirectories: true,
  };
  const resourceDirs = await fastglob('**/__MACOSX', globOptions);
  await Promise.all(resourceDirs.map((resDir) => rm(resDir, { recursive: true })));
  return resourceDirs.length;
}

class IpaSigner {

  /** @type {string} */
  #provProfile;

  /** @type {string} */
  #devCert;

  /** @type {ProgressMeter} */
  #meter;

  /** @type {string} */
  #plistbuddyBinary;

  /** @type {string} */
  #securityBinary;

  /** @type {string} */
  #codesignBinary;

  /**
   * @param {string} provisioningProfile
   * @param {string} developerCertificate
   * @param {Object} options
   * @param {ProgressMeter} options.meter
   * @param {boolean} options.cleanupTemp
   * @param {string} options.plistbuddyBinary
   * @param {string} options.securityBinary
   * @param {string} options.codesignBinary
   */
  constructor(
    provisioningProfile,
    developerCertificate,
    {
      meter = new ProgressMeter(),
      cleanupTemp = true,
      plistbuddyBinary = PLISTBUDDY_BINARY,
      securityBinary = SECURITY_BINARY,
      codesignBinary = CODESIGN_BINARY,
    } = {},
  ) {
    this.#provProfile = provisioningProfile;
    this.#devCert = developerCertificate;
    this.#meter = meter;
    this.#plistbuddyBinary = plistbuddyBinary;
    this.#securityBinary = securityBinary;
    this.#codesignBinary = codesignBinary;

    if (cleanupTemp) {
      setGracefulCleanup();
    }
  }

  /**
   * Configures and re-signs a packed IPA
   * @param {string} ipa Path of the packed IPA to sign
   * @param {string} signedIpa Path to store the re-signed packed IPA
   * @param {import('../configurator/IpaConfigurator').default} ipaConfigurator IPA configurator
   * @returns {Promise<string>} Path to the re-signed IPA
   */
  async resignPackedIpa(ipa, signedIpa, ipaConfigurator) {
    const meter = this.#meter;

    meter.set(0.05, `Unpacking ${ipa}`);
    const { path: tmpIpaDir } = await tmpDir({ unsafeCleanup: true });
    const ipaPacker = new IpaPacker();
    await ipaPacker.unpack(ipa, tmpIpaDir);

    meter.set(0.15, 'Removing __MACOSX folders, if any', false);
    const removedMacosxFolders = await removeMacOsResourceDirs(tmpIpaDir);
    if (removedMacosxFolders > 0) {
      meter.set(0.16, `Removed ${removedMacosxFolders} __MACOSX folders`);
    }

    meter.set(0.17, 'Configure IPA');
    await this.#configureIpaPayload(tmpIpaDir, ipaConfigurator, meter.partial(0.26));

    meter.set(0.27, 'Start signing');
    await this.#resignIpa(tmpIpaDir, meter.partial(0.48));
    meter.set(0.49, 'Done signing');

    meter.set(0.5, 'Compressing to temporary IPA file');
    const tmpSignedIpa = await this.#pack(tmpIpaDir, meter.partial(0.97));
    meter.set(0.98, `Compressed to ${parse(tmpSignedIpa).base}`);

    meter.set(0.99, 'Move IPA to final location', false);
    await rename(tmpSignedIpa, signedIpa);
    meter.set(1.0, `Created ${signedIpa}`);

    return signedIpa;
  }

  /**
   * @param {string} tmpIpaDir
   * @param {ProgressMeter} meter
   * @returns {Promise<string>} Path to temporary IPA file
   */
  // eslint-disable-next-line class-methods-use-this
  async #pack(tmpIpaDir, meter) {
    const { path: tmpSignedIpa } = await tmpFile({ postfix: '.ipa' });

    const ipaPacker = new IpaPacker();
    await ipaPacker.packRecursive(tmpIpaDir, tmpSignedIpa, meter);

    return tmpSignedIpa;
  }

  /**
   * Configures an extracted IPA
   * @param {string} ipaDir Path ot the extracted IPA to re-sign
   * @param {import('../configurator/IpaConfigurator').default} ipaConfigurator IPA configurator
   * @param {ProgressMeter} meter
   */
  async #configureIpaPayload(ipaDir, ipaConfigurator, meter) {
    meter.set(0.05, 'Finding app directory in payload folder', false);
    const payloadAppDir = await getFirstPayloadAppDir(ipaDir);
    meter.set(0.1, `Found ${parse(payloadAppDir).base}`);

    meter.set(0.3, 'Applying IPA type specific configuration');
    await ipaConfigurator.run(payloadAppDir, meter.partial(0.9));

    meter.set(1.0, 'Copying provisioning profile into application payload');
    const embedProvProfile = join(payloadAppDir, 'embedded.mobileprovision');
    await copyFile(this.#provProfile, embedProvProfile);
  }

  /**
   * Re-signs an extracted IPA
   * @param {string} ipaDir Path ot the extracted IPA to re-sign
   * @param {ProgressMeter} meter
   */
  async #resignIpa(ipaDir, meter) {
    meter.set(0.05, 'Extracting entitlements from provisioning profile');
    const entitlementPlist = await this.#extractEntitlementsFromProvProfile();

    meter.set(0.1, `Re-sign components with certificate "${this.#devCert}"`);
    await this.#signComponents(ipaDir, entitlementPlist, meter.partial(1.0));
  }

  /**
   * Executes a plistbuddy command on file
   * @param {string} file Path to PList file
   * @param {string} command
   * @param {string} property
   * @param {string} value
   * @param {string} type
   * @returns {Promise<string>}
   */
  async #plistbuddy(file, command, property, value = '', type = '') {
    const args = ['-x', '-c', `${command} ${property} ${type} ${value}`.trim(), file];
    const { stdout } = await execFile(this.#plistbuddyBinary, args);
    return stdout;
  }

  /**
   * Finds all sign-worthy elements and re-signs them
   * @param {string} ipaDir Path ot the extracted IPA to re-sign
   * @param {?string} entitlementPlist Path of a plist file with entitlements
   * @param {ProgressMeter} meter
   */
  async #signComponents(ipaDir, entitlementPlist, meter) {
    const globPattern = '**/*.@(app|appex|framework|dylib)';
    const globOptions = {
      cwd: ipaDir,
      absolute: true,
      onlyFiles: false,
      onlyDirectories: false,
    };
    const components = await fastglob(globPattern, globOptions);
    // eslint-disable-next-line no-restricted-syntax
    for (const [index, component] of components.entries()) {
      // eslint-disable-next-line no-await-in-loop
      await this.#codesign(component, entitlementPlist);
      meter.set(index / components.length, `Sign ${parse(component).base}`, false);
    }
  }

  /**
   * Signs a single given component
   * @param {string} componentPath Path to the component to re-sign
   * @param {?string} entitlementPlist Path of a PList file with entitlements
   */
  async #codesign(componentPath, entitlementPlist = null) {
    const args = ['--generate-entitlement-der', '--continue', '-f', '-s', this.#devCert];
    if (entitlementPlist) {
      args.push('--entitlements', entitlementPlist);
    }
    args.push(componentPath);
    await execFile(this.#codesignBinary, args);
  }

  /**
   * Extracts the entitlements from the provisioning profile into a temporary PList
   * @returns {Promise<string>} Path of a temporary PList file with entitlements
   */
  async #extractEntitlementsFromProvProfile() {
    const { path: provisioningPlist } = await tmpFile({ prefix: 'provisioning', postfix: '.plist' });
    const { stdout: securityCmsOutput } = await execFile(this.#securityBinary, ['cms', '-D', '-i', this.#provProfile]);
    await writeFile(provisioningPlist, securityCmsOutput);

    const { path: entitlementPlist } = await tmpFile({ prefix: 'entitlements', postfix: '.plist' });
    const entitlements = await this.#plistbuddy(provisioningPlist, 'print', ':Entitlements');
    await writeFile(entitlementPlist, entitlements);

    return entitlementPlist;
  }

}

export default IpaSigner;
