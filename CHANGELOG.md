# Changelog

## [5.0.5] 2022-02-09

### Fixed
- Summary of failed deployments sorted by device name (#6)

## [5.0.4] 2022-02-09

### Changed
- Show summary of failed deployments after interactive deploy action (#6)

## [5.0.3] 2022-02-09

### Changed
- Show default IPA type as hint next to the device name when deploying (#5)

## [5.0.2] Initial public release
